# The idea
Every time you open an app, a website, or a piece of software for the first time shown you a multi-page set of terms and conditions that you are expected to read and agree to without discussion or negotiation.

It should not be like this. In this project we introduce the idea of "Open Terms", where you select the way a company or person should behave with you. The default position is privacy and fairness. 

The way it works is this: 
  * You set up a basic set of terms that you are prepared to be the default terms.
  * When you log on to a website or an app, this app will use your open terms profile to know what it can and cannot do.
  * This may cause functionality to be limited, but that's OK, it's you who decides
  * Open terms can be switched on and off at any time by you for any app, any website or software
  * Open Terms are just that - OPEN. Free for inspection by everyone.


Example: when a company wants to have access to your private information, a picture, they must interrogate your Open Terms profile to understand what functionality they are allowed to apply. If they apply more functionaly than you have agree, they will be breaking the terms.

The purpose of this project is to work out and develop how this can be achieved. It is one of the key components of the **Government Reimagined** movement which aims to create parallel global level government for the people. It will set milestones, deal with issues and set projects. 

## Initial version
The first iteration of this project is a fact-finding mission to define the format of the terms. *All participants welcome in the discussion.* 